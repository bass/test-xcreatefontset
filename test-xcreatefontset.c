/*
 * $Id$
 */

#include <stdio.h>

#include <locale.h>
#include <langinfo.h>

#include <X11/Xlib.h>
#include <X11/Intrinsic.h>
#include <X11/Xatom.h>

int main() {
	const char *locale = setlocale(LC_ALL, "");
	if (locale == NULL) {
		fprintf(stderr, "Locale unavailable.\n");
	}

	char *localeCharset = nl_langinfo(CODESET);
	printf("locale: %s\n", locale);
	printf("run-time charset: %s\n", localeCharset);

	XtSetLanguageProc(NULL, (XtLanguageProc) NULL, (XtPointer) NULL);

	Display* dpy = XOpenDisplay(NULL);

	char ** missingCharsets;
	int missingCharsetCount = 0;
	char * defaultString;
	XFontSet fontSet = XCreateFontSet(dpy,
			"-monotype-arial-medium-r-normal--*-90-*-*-p-0-*-*, \
			-monotype-arial-regular-r-normal--*-90-*-*-p-0-*-*", // Xsun
			&missingCharsets,
			&missingCharsetCount,
			&defaultString);

	printf("X11 locale: %s\n", XLocaleOfFontSet(fontSet));
	printf("XFontSet: %s\n", XBaseFontNameListOfFontSet(fontSet));

	if (missingCharsetCount > 0) {
		printf("Charsets missing from XFontSet:\n");
		/*
		 * For Arial in UTF-8 locale, glyphs for only the following
		 * charsets are missing (irrelevant for Cyrillic):
		 *
		 *	- ISO8859-14
		 *	- JISX0208.1983-0
		 *	- KSC5601.1987-0
		 *	- GB2312.1980-0
		 *	- JISX0201.1976-0
		 */
		for (int i = 0; i < missingCharsetCount; i++) {
			char * missingCharset = *(missingCharsets + i);
			printf("\t%s\n", missingCharset);
		}
		XFreeStringList(missingCharsets);
	}

	printf("XFontStruct entries in XFontSet:\n");

	XFontStruct** fonts;
	char ** fontNames;
	int count = XFontsOfFontSet(fontSet, &fonts, &fontNames);
	for (int i = 0; i < count; i++) {
		XFontStruct * const font = *(fonts + i);
		char * const fallbackFontName = *(fontNames + i);


		unsigned long fontNameAtom;
		if (!XGetFontProperty(font, XA_FONT, &fontNameAtom)) {
			printf("\tXGetFontProperty(XA_FONT) failed\n");
			printf("\t%s\n", fallbackFontName);
		} else {
			char * const fontName = XGetAtomName(dpy, (Atom) fontNameAtom);
			printf("\t%s\n", fontName);
			XFree(fontName);
		}
	}

	XCloseDisplay(dpy);

	return 0;
}
