CFLAGS = --std=c99 -O0 -g3 -pedantic -Wall -Wextra -Wconversion
LDLIBS = -lXt -lX11

ifdef COMSPEC
EXESUFFIX = .exe
endif

.PHONY: all
all: test-xcreatefontset

test-xcreatefontset: test-xcreatefontset.o

.PHONY: clean
clean:
	$(RM) *.o test-xcreatefontset$(EXESUFFIX)
